import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JProgressBar;
import java.awt.Font;

public class View extends JFrame {

	private JPanel contentPane;
	private JTextField txtValorMoneda;
	private JTextField txtPaga;
	private JTextField txtCantParaGanar;
	private JTextField txtCantSimulaciones;
	private JTextField txtGanados;
	private JTextField txtPerdidos;
	private JTextField txtEmpatados;
	private JTextField txtProm;
	private Simulador simulador;
	private JTextField txtGanancia;
	private JProgressBar progressBar;
	private View instance;
	private JButton btnSimular;
	private JButton btnVerGrfico;
	private Graficador graficador;
	private JButton btnVerRegistro;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View frame = new View();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public View() {
		this.instance = this;
		this.setResizable(false);
		graficador = new Graficador();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 275, 466);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[140.00][233.00]", "[][][][][][][18.00][][][][][][][]"));
		
		JLabel lblParmetrosDeEntrada = new JLabel("Par\u00E1metros de Entrada");
		lblParmetrosDeEntrada.setForeground(Color.GRAY);
		contentPane.add(lblParmetrosDeEntrada, "cell 0 0");
		
		JLabel lblValorDeLa = new JLabel("Valor de la moneda:   $");
		lblValorDeLa.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblValorDeLa, "cell 0 1,alignx trailing");
		
		txtValorMoneda = new JTextField();
		contentPane.add(txtValorMoneda, "cell 1 1,alignx left");
		txtValorMoneda.setColumns(10);
		
		JLabel lblGananciaAlFinalizar = new JLabel("Ganancia al finalizar:   $");
		lblGananciaAlFinalizar.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblGananciaAlFinalizar, "cell 0 2,alignx trailing");
		
		txtPaga = new JTextField();
		contentPane.add(txtPaga, "cell 1 2,alignx left");
		txtPaga.setColumns(10);
		
		JLabel lblCantidadParaGanar = new JLabel("Cantidad para ganar:");
		lblCantidadParaGanar.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblCantidadParaGanar, "cell 0 3,alignx trailing");
		
		txtCantParaGanar = new JTextField();
		contentPane.add(txtCantParaGanar, "cell 1 3,alignx left");
		txtCantParaGanar.setColumns(10);
		
		JLabel lblSimulaciones = new JLabel("Simulaciones:");
		lblSimulaciones.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblSimulaciones, "cell 0 4,alignx trailing");
		
		txtCantSimulaciones = new JTextField();
		contentPane.add(txtCantSimulaciones, "cell 1 4,alignx left");
		txtCantSimulaciones.setColumns(10);
		
		btnSimular = new JButton("Simular");
		btnSimular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//public Simulador(View vista, int cantSimulaciones, int cantParaGanar, int valorMoneda, int valorVictoria)
				if(datosValidos()){
					try {
						btnSimular.setEnabled(false);
						simulador = new Simulador(Integer.parseInt(txtCantSimulaciones.getText()), Integer.parseInt(txtCantParaGanar.getText()), Integer.parseInt(txtValorMoneda.getText()), Integer.parseInt(txtPaga.getText()));
						SimManager sm = new SimManager(simulador, instance);
						if((Integer.parseInt(txtCantSimulaciones.getText()) < 10) && (Integer.parseInt(txtCantParaGanar.getText()) < 5)){
							sm.setRegistrar(true);
						}
						Thread smThread = new Thread(sm);
						smThread.start();
						simulador.setSm(sm);
						btnVerGrfico.setEnabled(false);
						btnVerRegistro.setEnabled(false);
					} catch (Exception e) {
						btnSimular.setEnabled(true);
						JOptionPane.showMessageDialog(null, "Error de datos");
					}
				}else{
					JOptionPane.showMessageDialog(null, "Error de datos");
				}

			}

			private boolean datosValidos() {
				try {
					if(Integer.parseInt(txtCantSimulaciones.getText()) > 0){
						if(Integer.parseInt(txtCantParaGanar.getText()) > 0){
							if(Integer.parseInt(txtValorMoneda.getText()) > 0){
								if(Integer.parseInt(txtPaga.getText()) > 0){
									return true;
								}
							}	
						}
					}
					return false;
				} catch (Exception e) {
					return false;
				}
			}
		});
		contentPane.add(btnSimular, "cell 0 5,alignx right");
		
		progressBar = new JProgressBar();
		contentPane.add(progressBar, "cell 1 5,alignx left");
		
		JLabel lblSalidaDelPrograma = new JLabel("Salida del Programa");
		lblSalidaDelPrograma.setForeground(Color.GRAY);
		contentPane.add(lblSalidaDelPrograma, "cell 0 7");
		
		JLabel lblJuegosGanados = new JLabel("Juegos ganados:");
		contentPane.add(lblJuegosGanados, "cell 0 8,alignx trailing");
		
		txtGanados = new JTextField();
		txtGanados.setEditable(false);
		contentPane.add(txtGanados, "cell 1 8,alignx left");
		txtGanados.setColumns(10);
		
		JLabel lblJuegosPerdidos = new JLabel("Juegos perdidos:");
		contentPane.add(lblJuegosPerdidos, "cell 0 9,alignx trailing");
		
		txtPerdidos = new JTextField();
		txtPerdidos.setEditable(false);
		contentPane.add(txtPerdidos, "cell 1 9,alignx left");
		txtPerdidos.setColumns(10);
		
		JLabel lblJuegosEmpatados = new JLabel("Juegos empatados:");
		contentPane.add(lblJuegosEmpatados, "cell 0 10,alignx trailing");
		
		txtEmpatados = new JTextField();
		txtEmpatados.setEditable(false);
		contentPane.add(txtEmpatados, "cell 1 10,alignx left");
		txtEmpatados.setColumns(10);
		
		JLabel lblPromedioDe = new JLabel("Promedio de Tiros:");
		contentPane.add(lblPromedioDe, "cell 0 11,alignx trailing");
		
		txtProm = new JTextField();
		txtProm.setEditable(false);
		contentPane.add(txtProm, "cell 1 11,alignx left");
		txtProm.setColumns(10);
		
		JLabel lblGanancia = new JLabel("Ganancia:   $");
		lblGanancia.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblGanancia, "cell 0 12,alignx trailing");
		
		txtGanancia = new JTextField();
		txtGanancia.setEditable(false);
		contentPane.add(txtGanancia, "cell 1 12,alignx left");
		txtGanancia.setColumns(10);
		
		btnVerGrfico = new JButton("Ver gr\u00E1fico");
		btnVerGrfico.setEnabled(false);
		btnVerGrfico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				graficador.graficar(Integer.parseInt(txtGanados.getText()), Integer.parseInt(txtEmpatados.getText()), Integer.parseInt(txtPerdidos.getText()));
			}
		});
		
		btnVerRegistro = new JButton("Ver registro");
		btnVerRegistro.setToolTipText("V�lido cuando la cantidad de simulaciones es menor a 10 y la cantidad para ganar es menor a 5");
		btnVerRegistro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Registro r = new Registro();
				r.setText(simulador.registro.toString());
				r.setVisible(true);
			}
		});
		btnVerRegistro.setEnabled(false);
		contentPane.add(btnVerRegistro, "cell 0 13,alignx right");
		contentPane.add(btnVerGrfico, "cell 1 13,alignx left");
	}
	
	public void setProgress(int value){
		progressBar.setValue(value);
		if(value == 100){
			btnSimular.setEnabled(true);
		}
	}
	public void setData(){
		txtGanados.setText(String.valueOf(simulador.ganados));
		txtPerdidos.setText(String.valueOf(simulador.perdidos));
		txtEmpatados.setText(String.valueOf(simulador.empatados));
		txtProm.setText(String.valueOf(simulador.promedioTiros));
		txtGanancia.setText(String.valueOf(simulador.ganancia));
	}

	public void finSimulacion(boolean registrar) {
		setData();
		btnVerGrfico.setEnabled(true);
		if(registrar){
			btnVerRegistro.setEnabled(true);
		}
	}

}
