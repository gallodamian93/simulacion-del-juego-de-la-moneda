## Sobre el juego

El juego de la moneda consiste seleccionar un lado *L* de la moneda y arrojarla *N* veces hasta que salga consecutivamente *C* veces.

Cada tirada de la moneda tiene un costo fijo asociado y al finalizar el tirador gana el monto definido al iniciar el juego.

Ejemplo:

Costo de tirada: $1

Ganancia al finalizar: $8

C: 3

L: cara

Al final finalizar el juego se hace un seguimiento y el registro de tiradas arroja el siguiente resultado:

cara-cara-ceca-cara-cara-cara

Ganancia total = $8-5*$1 = $3

## Sobre la simulación

El programa permite seleccionar los parámetros mencionados del juego y la cantidad de simulaciones a ejecutar. En la salida del mismo se observarán datos estadísticos de las simulaciones (ver *salida*).