
public class SimManager implements Runnable{
	Simulador sim;
	View vista;
	boolean registrar;
	
	public SimManager(Simulador sim, View vista){
		this.sim = sim;
		this.vista = vista;
		this.registrar = false;
	}
	@Override
	public void run() {
		sim.simular(registrar);
		vista.finSimulacion(registrar);
	}
	public void setRegistrar(boolean registrar){
		this.registrar = registrar;
	}
	
	public void setProgress(int progress){
		vista.setProgress(progress);
	}

}
