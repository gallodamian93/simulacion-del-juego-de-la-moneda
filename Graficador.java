import java.text.DecimalFormat;
import javax.swing.JFrame;
import org.jfree.chart.ChartFactory;	 
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;


public class Graficador {
	
	public void graficar(int ganados, int empatados, int perdidos){
		DefaultPieDataset dataset = new DefaultPieDataset( );
		dataset.setValue( "Ganados" , ganados);  
		dataset.setValue( "Empatados" , empatados);   
		dataset.setValue( "Perdidos" , perdidos); 
		
		
		JFreeChart chart = ChartFactory.createPieChart3D("Porcentaje de juegos ganados, perdidos y empatados", dataset, true, true, false);
		


		PiePlot plot = (PiePlot)chart.getPlot();
		plot.setLabelGenerator(new StandardPieSectionLabelGenerator("{2}",new DecimalFormat("0"), new DecimalFormat("0.0%")));
		ChartPanel cPanel = new ChartPanel(chart);
		JFrame information = new JFrame("Gr�fico");
		information.getContentPane().add(cPanel);
		information.pack();
		information.setVisible(true);
	}

}
