import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class main {
	public static void main(String[] args) {
	    try {
	            // Set cross-platform Java L&F (also called "Metal")
	        UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    }
	    catch (ClassNotFoundException e) {
	       // handle exception
	    }
	    catch (InstantiationException e) {
	       // handle exception
	    }
	    catch (IllegalAccessException e) {
	       // handle exception
	    }
	        
	    View view = new View();
	    view.setVisible(true);
	}


}
