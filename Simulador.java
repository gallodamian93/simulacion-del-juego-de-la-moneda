
public class Simulador {
	int cantSimulaciones;
	int cantParaGanar;
	int valorMoneda;
	int valorVictoria;
	SimManager manager;
	
	int ganados;
	int empatados;
	int perdidos;
	float promedioTiros;
	int ganancia;
	StringBuilder registro;
	
	
	public Simulador(int cantSimulaciones, int cantParaGanar, int valorMoneda, int valorVictoria) {
		super();
		this.cantSimulaciones = cantSimulaciones;
		this.cantParaGanar = cantParaGanar;
		this.valorMoneda = valorMoneda;
		this.valorVictoria = valorVictoria;
		this.ganados = 0;
		this.empatados = 0;
		this.perdidos = 0;
		this.promedioTiros = 0;
		this.ganancia = 0;
		registro = new StringBuilder();
	}
	
	public void simular(boolean registrar) {
		int acumulador = 0;
		int gananciaParcial;
		if(registrar){
			registro.append("REGISTRO DE SIMULACIONES REALIZADAS\n");
		}
		for(int i = 0; i < cantSimulaciones; i++){
			int cara = 0;
			int ceca = 0;
			int tiros = 0;
			//inicia simulacion
			if(registrar){
				registro.append("\n Simulacion ");
				registro.append(i+1); 
				registro.append(": ");
			}
			while(true){
				if(Math.random() >= 0.5){
					cara++;
					if(registrar){
						registro.append(1);
					}
				} else {
					ceca++;
					if(registrar){
						registro.append(0);
					}
				}
				tiros++;
				if(tiros >= this.cantParaGanar){
					if(((cara-ceca) == this.cantParaGanar)||((cara-ceca) == -this.cantParaGanar)){
						//finalizo el juego
						gananciaParcial = this.valorVictoria - (tiros*this.valorMoneda);
						ganancia += gananciaParcial;
						if((tiros*this.valorMoneda) < this.valorVictoria){
							//perdio
							this.ganados++;
							if(registrar){
								registro.append(" -> Ganado[Ganancia = $");
								registro.append(gananciaParcial + "]");
							}
								break;
						} else if((tiros*this.valorMoneda) > this.valorVictoria){
							//gano
							this.perdidos++;
							if(registrar){
								registro.append(" -> Perdido[Ganancia = $");
								registro.append(gananciaParcial + "]");
							}
							break;
						} else {
							//empato
							this.empatados++;
							if(registrar){
								registro.append(" -> Empatado[Ganancia = $");
								registro.append(gananciaParcial + "]");
							}
							break;
						}
					} 
				}
			}
			acumulador += tiros;
			manager.setProgress((int)(((float)i/cantSimulaciones)*100));
		}
		if(registrar){
			registro.append("\nFIN DE LA SIMULACION");
		}
		manager.setProgress(100);
		this.promedioTiros = acumulador/this.cantSimulaciones;
	}

	public void setSm(SimManager sm) {
		// TODO Auto-generated method stub
		this.manager = sm;
	}
	
}
